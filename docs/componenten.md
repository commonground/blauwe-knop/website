---
id: componenten
title: Componenten en beproeven
sidebar_label: Componenten en beproeven
---

## Referentie-implementaties/sdk

Van de softwarecomponenten die nodig zijn om het Blauwe Knop protocol uit te voeren, zijn referentie-implementaties ontwikkeld. Deze zijn onder open source licentie beschikbaar en te vinden op [Gitlab](https://gitlab.com/commonground/blauwe-knop). Deelnemende organisaties kunnen deze componenten zelf (laten) installeren/implementeren in hun applicatielandschap.

### Overzicht alle componenten

In het totale ecosysteem zijn, geplot op het vijf-lagenmodel van Common Ground, de volgende componenten relevant:

![](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/BK-varianten-alle-componenten.png)

In deze plaat zijn per rol de verschillende componenten aangegeven. Alle componenten zijn beschikbaar als docker-containers. Van een aantal componenten is duidelijk dat deze verder opgesplitst kunnen worden om beter te passen in lokale situaties bij gemeenten, bijvoorbeeld als schermen worden geïntegreerd in een MijnGemeente-omgeving.

Voor meer informatie over de status en beschikbaarheid van componenten, zie ook de [Common Ground Appstore](https://appstore.commonground.nl/).

### Proeftuin-opstelling en beproevingen

In de proeftuinomgeving bij VNG Realisatie draait op dit moment een proeftuinopstelling op basis van de referentie-implementaties. In deze proefopstelling worden dertien overheidsorganisaties gevirtualiseerd op een [Haven](https://haven.commonground.nl/) omgeving. Haven maakt het mogelijk om software zo te packagen dat deze op alle moderne infrastructuren kan draaien.

**Beproevingen**

Op dit moment worden beproevingen met de Blauwe Knop Basis 2.0 voorbereid.
In overleg met VNG Realisatie kunnen (overheids)organisaties daaraan deelnemen.

Bij een beproeving zijn (technisch) de volgende stappen benodigd:

- Overleg met VNG Realisatie over de scope van de beproeving (als bronorganisatie, als registrator, etc.)
- Overleg met team Blauwe Knop over de (technische) vereisten voor deelname.
- Deelnemende (overheids)organisaties moeten zelf (evt in samenwerking met hun leverancier) de benodigde componenten installeren/implementeren in hun applicatielandschap.
- VNG Realisatie kan in overleg ondersteuning bieden bij beproevingen, en waar nodig ook aanpassingen maken aan het procol of referentie-implementaties.
- VNG Realisatie voegt de deelnemende organisatie toe aan de lijst van deelnemende bronorganisaties.
- Gebruikers van de app kunnen nu hun schuldinformatie ophalen bij de deelnemende bronorganisatie.
