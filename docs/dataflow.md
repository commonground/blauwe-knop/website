---
id: dataflow
title: Dataflow
sidebar_label: Dataflow
---

## Burger, bronorganisaties, bronsystemen

Bij de uitvoering van Blauwe Knop zijn verschillende partijen betrokken. In de basis zijn behalve de burger zelf, de (overheids)organisaties betrokken waar de burger schulden kan hebben (bronorganisaties). Bij deze bronorganisaties is schuldinformatie niet altijd in één systeem beschikbaar, maar ook vaak verspreid over meerdere bronsystemen. Bronsystemen worden bovendien regelmatig door andere partijen dan de bronorganisatie beheerd. Deze partijen treden in dat geval op als verwerker voor die gegevens. Deze verwerking is een zaak van de bronorganisaties en geen onderdeel van het Blauwe Knop protocol. Voor het Blauwe Knop protocol maakt het niet uit of bronsystemen bij de bronorganisatie zelf staan of bij andere organisaties, mits deze maar bereikbaar zijn voor de bronorganisatie, bijvoorbeeld via NLX.

![Dataflow](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/burger-bronorganisaties-bronsystemen.png)

### Afbeelding dataflow

![Dataflow](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/dataflow.png)