---
id: introductie
title: Introductie
sidebar_label: Introductie
---

## Over de Proeftuin Blauwe Knop

In de proeftuin Blauwe Knop voeren we R&D uit met betrekking tot regie op gegevens met behulp van de Blauwe Knop. Doel is te onderzoeken hoe het voor burgers makkelijker gemaakt kan worden om informatie over hun schulden bij verschillende (overheids)organisaties te verzamelen. In de praktijk is het overzicht krijgen over schulden namelijk een grote drempel bij  de start van schuldhulpverlening.

We ontwikkelen hiervoor een (demo-)app waarmee burgers een geaggregeerd overzicht kunnen krijgen van de schulden die een burger bij de overheid heeft. Doel van de eerste fase van de proeftuin is het ontwikkelen van een integraal overzicht in de app van alle schulden bij de aangesloten organisaties, waarbij er slechts eenmaal behoeft te worden ingelogd, en op basis van éénmalig inloggen alle data bij de bronnen wordt opgehaalden getoond in het overzicht.

Binnen project de Blauwe Knop worden drie plateau's van functionaliteit voorzien:
- Blauwe Knop Basis (burgers kunnen bij afzonderlijke organisaties pdf-overzichten van schuldinformatie ophalen)
- **Blauwe Knop Basis 2.0 (burgers kunnen een geaggregeerd overzicht van schuldinformatie digitiaal verzamelen in een app)**
- Blauwe Knop Plus (burgers kunnen regie op gegevens uitoefenen op over hun schuldinformatie, zodat afnemende partijen (bijvoorbeeld schuldhulpverleningssoftware, deze rechtstreeks van bron(nen) kan ophalen)

**De proeftuin richt zich op dit moment op plateau 2, 'Blauwe Knop Basis 2.0', waarbij de burger zelf in een app een geaggregeerd overzicht van schuldinformatie kan verzamelen.**

### Werkwijze Proeftuin

De ontwikkeling van de oplossing zal iteratief worden uitgevoerd volgens de principes van Common Ground, en is open te volgen op [Gitlab](https://gitlab.com/commonground/blauwe-knop). 

### Screenshots

![Screenshot startscherm](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/ui/1-2-startscherm.png)

### BPMN processen

![BPMN procesplaat fase 2](https://gitlab.com/commonground/blauwe-knop/website/-/raw/master/static/img/bpmn-totaal-variant-2.png)