module.exports = {
  title: 'Proeftuin Blauwe Knop',
  tagline: '',
  url: 'https://blauweknop.app',
  baseUrl: '/',
  favicon: 'img/favicon.ico',
  organizationName: 'vng-realisatie', // Usually your GitHub org/user name.
  projectName: 'blauwe-knop-website', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Blauwe Knop',
      logo: {
        alt: 'Blauwe Knop',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/introductie',
          activeBasePath: 'docs',
          label: 'Documentatie',
          position: 'left',
        },
        {
          href: 'https://gitlab.com/commonground/blauwe-knop',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      copyright: `Copyright © ${new Date().getFullYear()} VNG Realisatie.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/commonground/blauwe-knop/website/edit/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
