module.exports = {
  someSidebar: {
    'Documentatie': [
      'introductie',
      // 'klantreis',
      'dataflow',
      'processen',
      'ux-ui',
      'protocol',
      'componenten',
      'aan-de-slag',
    ]
  },
};
