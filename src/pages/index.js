import React from "react";
import classnames from "classnames";
import Layout from "@theme/Layout";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout
      title={siteConfig.title}
      description="Informatie over de Proeftuin Blauwe Knop"
    >
      <header className={classnames("hero hero--primary", styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={classnames("button button--lg", styles.getStarted)}
              to={useBaseUrl("docs/introductie")}
            >
              Bekijk documentatie
            </Link>
          </div>
        </div>
      </header>
      <section
        className={classnames("hero hero--secondary", styles.heroBanner)}
      >
        <div className="container">
          <Link
            className={classnames("button button--lg button--secondary")}
            to={useBaseUrl("/android/blauweknop-0.10.0.apk")}
            target="_blank"
            download
          >
            Download Blauwe Knop Android
          </Link>
        </div>
      </section>
    </Layout>
  );
}

export default Home;
